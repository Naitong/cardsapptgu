package com.realquest.android.cardsapptgu.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.realquest.android.cardsapptgu.AddCard
import com.realquest.android.cardsapptgu.R


/**
 * A simple [Fragment] subclass.
 */
class FragmentCards : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_fragment_cards, container, false)

        val addCard: Button? = view?.findViewById(R.id.addCard)

        addCard?.setOnClickListener {
            val intent5 = Intent(activity, AddCard::class.java)
            startActivity(intent5)
        }
        return view
    }

}
