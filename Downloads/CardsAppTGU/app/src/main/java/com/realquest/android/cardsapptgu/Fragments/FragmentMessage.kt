package com.realquest.android.cardsapptgu.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.realquest.android.cardsapptgu.R


/**
 * A simple [Fragment] subclass.
 */
class FragmentMessage : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_message, container, false)
    }

}
