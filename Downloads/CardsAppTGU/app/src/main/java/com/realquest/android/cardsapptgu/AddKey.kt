package com.realquest.android.cardsapptgu

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

/**
 * Created by Nugaev Marat on 14.08.2018.
 */

class AddKey: AppCompatActivity(){

    lateinit var chooseCard:EditText
    lateinit var chooseObject:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_key)

        chooseCard = findViewById(R.id.chooseCard)
        chooseObject = findViewById(R.id.chooseObject)

        val sv2Button = findViewById<Button>(R.id.sv2Button)
        sv2Button.setOnClickListener {
            if (chooseCard.text.isEmpty() || chooseObject.text.isEmpty()){
                Toast.makeText(this, "Одно из полей не заполненно", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Ключ сохранен", Toast.LENGTH_LONG).show()
            }
        }
    }
}
