package com.realquest.android.cardsapptgu

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.realquest.android.cardsapptgu.Fragments.FragmentCards
import com.realquest.android.cardsapptgu.Fragments.FragmentKey
import com.realquest.android.cardsapptgu.Fragments.FragmentMessage
import com.realquest.android.cardsapptgu.Fragments.ProfileFragment
import kotlinx.android.synthetic.main.activity_profile.*


/**
 * Created by Nugaev Marat on 14.08.2018.
 */
class ProfileActivity : AppCompatActivity() {

    val manager = supportFragmentManager

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_key -> {
                createKeyFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_message -> {
                createMessageFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_cards -> {
                createCardsFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        createProfile()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    fun createProfile(){
        val transaction = manager.beginTransaction()
        val fragment = ProfileFragment()
        transaction.replace(R.id.flProfileContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    
    fun createKeyFragment() {
        val transaction = manager.beginTransaction()
        val fragment = FragmentKey()
        transaction.replace(R.id.flProfileContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun createMessageFragment() {
        val transaction = manager.beginTransaction()
        val fragment = FragmentMessage()
        transaction.replace(R.id.flProfileContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun createCardsFragment() {
        val transaction = manager.beginTransaction()
        val fragment = FragmentCards()
        transaction.replace(R.id.flProfileContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
