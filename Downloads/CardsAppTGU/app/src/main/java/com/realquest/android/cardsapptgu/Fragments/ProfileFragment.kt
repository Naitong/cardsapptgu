package com.realquest.android.cardsapptgu.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.realquest.android.cardsapptgu.R


/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_profile, container, false)

        val svBtn: Button? = view?.findViewById(R.id.svBtn)

        svBtn?.setOnClickListener {
          Toast.makeText(activity, "Данные сохранены", Toast.LENGTH_LONG).show()
        }
        return view
    }

}
