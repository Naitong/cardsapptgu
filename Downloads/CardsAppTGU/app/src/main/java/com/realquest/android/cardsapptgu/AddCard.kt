package com.realquest.android.cardsapptgu

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

/**
 * Created by Nugaev Marat on 14.08.2018.
 */

class AddCard : AppCompatActivity() {

    lateinit var cardNumber: EditText
    lateinit var endDate: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_card)

        cardNumber = findViewById(R.id.cardNumber)
        endDate = findViewById(R.id.endDate)

        val svButton = findViewById<Button>(R.id.svButton)

        svButton.setOnClickListener {
            if (cardNumber.text.isEmpty() || endDate.text.isEmpty()){
                Toast.makeText(this, "Одно из полей не заполненно", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Карта сохранена", Toast.LENGTH_LONG).show()
            }
        }
    }
}
