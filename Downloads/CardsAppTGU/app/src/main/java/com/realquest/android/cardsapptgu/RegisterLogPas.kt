package com.realquest.android.cardsapptgu

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

/**
 * Created by Nugaev Marat on 14.08.2018.
 */
class RegisterLogPas :AppCompatActivity(){

    lateinit var registerLogin:EditText
    lateinit var registerPassword:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_log_pas)

        val nextButton = findViewById<Button>(R.id.nextButton)

        registerLogin = findViewById(R.id.registerLogin)
        registerPassword = findViewById(R.id.registerPassword)

        nextButton.setOnClickListener(){
            val intent4 = Intent(this,   ProfileActivity::class.java)
            if(registerLogin.text.isEmpty() || registerPassword.text.isEmpty()){
                Toast.makeText(this, "Поле Email или Password не заполнено", Toast.LENGTH_LONG).show()
            }
            else{
                startActivity(intent4)
            }
        }


    }
}