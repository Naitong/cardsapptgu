package com.realquest.android.cardsapptgu

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

/**
 * Created by Nugaev Marat on 14.08.2018.
 */
class RegisterData : AppCompatActivity() {

    lateinit var registerSecondName: EditText
    lateinit var registerFirstName: EditText
    lateinit var registerThirdName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_data_activity)

        val regButton = findViewById<Button>(R.id.regButton)

        registerSecondName = findViewById(R.id.registerSecondName)
        registerFirstName = findViewById(R.id.registerFirstName)
        registerThirdName = findViewById(R.id.registerThirdName)

        regButton.setOnClickListener {
            val intent3 = Intent(this, RegisterLogPas::class.java)
            if (registerSecondName.text.isEmpty() || registerFirstName.text.isEmpty() ||
                    registerThirdName.text.isEmpty()) {
                Toast.makeText(this, "Одно из полей не заполненно", Toast.LENGTH_LONG).show()
            } else {
                startActivity(intent3)
            }
        }
    }
}