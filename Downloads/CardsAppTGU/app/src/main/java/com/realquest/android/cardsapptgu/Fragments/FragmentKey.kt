package com.realquest.android.cardsapptgu.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.realquest.android.cardsapptgu.AddKey

import com.realquest.android.cardsapptgu.R


/**
 * A simple [Fragment] subclass.
 */
class FragmentKey : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_fragment_key, container, false)

        val addKeyBtn:Button? = view?.findViewById(R.id.addKeyBtn)

        addKeyBtn?.setOnClickListener(){
            val intent6 = Intent(activity, AddKey::class.java)
            startActivity(intent6)
        }

        return view
    }

}
