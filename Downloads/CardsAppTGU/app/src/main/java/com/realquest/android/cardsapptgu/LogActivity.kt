package com.realquest.android.cardsapptgu

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

/**
 * Created by Nugaev Marat on 14.08.2018.
 */

class LogActivity : AppCompatActivity() {


    lateinit var editLogin:EditText
    lateinit var editPass:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.log_activity)

        val loginButton = findViewById<Button>(R.id.loginButton)
        val registTxt2 = findViewById<TextView>(R.id.registTxt2)
        editLogin = findViewById(R.id.editLogin)
        editPass = findViewById(R.id.editPass)

        loginButton.setOnClickListener {
            val intent = Intent(this, RegisterData::class.java)
            if (editLogin.text.isEmpty() || editPass.text.isEmpty()) {
                Toast.makeText(this, "Поля Логин или Пароль пустые", Toast.LENGTH_LONG).show()
            } else {
                startActivity(intent)
            }
        }

        registTxt2.setOnClickListener {
            val intent2 = Intent(this, RegisterData::class.java)
            startActivity(intent2)
        }
    }
}
